package com.stiltsoft.confluence.plugins.blueprint.markup;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.xhtml.api.EditorFormatService;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

import java.util.Map;

public class WikiMarkupProvider implements ContextProvider {

    private EditorFormatService editorFormatService;

    public WikiMarkupProvider(EditorFormatService editorFormatService) {
        this.editorFormatService = editorFormatService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> ctx) {
        try {
            String wikiMarkup = (String) ctx.get("wikiMarkup");
            String xhtml = editorFormatService.convertWikiToEdit(wikiMarkup, new DefaultConversionContext(new PageContext()));
            ctx.put("wikiInXhtml", xhtml);
        } catch (XhtmlException ignored) {
        }
        return ctx;
    }
}
